Master's Degree Testing Environment ([EN](./README.md)/PL)
==================================

**Master's Degree Testing Environment** jest wyspecjalizowanym narzędziem, które zostało stworzone dla pracy magisterskiej. To zaawansowane narzędzie jest zaprojektowane dla symulowania środowiska dla pojazdów autonopmicznych na torze wyścigowym. Narzędzie to wykorzystuje skierowane sieci neuronowe do sterowania pojazdamik oraz algorytmy ewolucyjne do trenowania. Wszystkie algorytmy SI zostały stworzone ręcznie na potrzeby tego narzędzia i nie są wykorzystywane zewnętrzne biblioteki do tego celu.



## Wymagania

Do budowania projektu z kodu źródłowego potrzebne są następujące składniki:

* Windows, Linux, macOS
* Unity 2018.1.0f2



## Demostracja

![Animation 1](.docs/animation1.gif)
![Animation 1](.docs/animation2.gif)
![Animation 1](.docs/animation3.gif)

## Zalecane ustawienia

Podczas badań w pracy dyplomowej najlepsze wartości parametrów został znalezione. Wartości te zostały zaprezentowane w poniższej tabeli:

| Funcka aktywacji | Warstwy ukrte | Minimalna prędkość | Długość czujników | Czas jednej generacji | Liczba osobników | Liczba generacji | Mutacje | Metoda selekcji osobników |
|:----------------:|:-------------:|:------------------:|:-----------------:|:---------------------:|:----------------:|:----------------:|:-------:|:-------------------------:|
|       Tanh       |     (6,6)     |         10         |         32        |        60 sek.        |        175       |        100       |    1%   |         Sześcienna        |



## Pobieranie

#### Pliki wykonywalne

Ostateczna wersja tego programu może być pobrana z podstrony tego repozytorium o nazwie **releases**. Ten projekt posiada pliki wykonywalne dla Linuxa, macOS oraz Windowsa, a więc budowanie ich z samodzielnie jest niepotrzebne.

#### Kod źródłowy

Możesz pobrać ostatnią wersję kodu tego narzędzia z gita. W celu uzyskania kodu źródłowego skorzystaj z komendy:
> gic clone [TODO: ADD LINK HERE]

Po pobrnaiu kodu źródłowego możesz zmodyfikować narzędzie i zbudować je na swoje środowisko.



## Autor

Autorem tego narzędzia jest Jakub Biliński. Możesz dowiedzieć się więcej na mojej stronie: [jbilinski.pl](http:/www.jbilinski.pl).