Master's Degree Testing Environment (EN/[PL](./README_PL.md))
==================================

**Master's Degree Testing Environment** is specialized tool created for master's degree thesis. It is advanced tool for simulating environment for autonomous vehicles on raced tracks. This tool use feedforward neural network for vehicle steering and evolutionary algorithms for training. Algorithms for AI were hand crafted and weren't used external libraries for this purpose. 



## Requirements

For building project from source code you need:

* Windows, Linux, macOS
* Unity 2018.1.0f2



## Demo

![Animation 1](.docs/animation1.gif)
![Animation 1](.docs/animation2.gif)
![Animation 1](.docs/animation3.gif)

## Recomended settings

During research of thesis best value for parameters were found. This values are presented in table below:

| Activation Function | Hidden Layers | Minimal Speed | Sensors Length | Timeout | Size | Count | Mutation | Parent Selection Method |
|:-------------------:|:-------------:|:-------------:|:--------------:|:-------:|:----:|:-----:|:--------:|:-----------------------:|
|         Tanh        |     (6,6)     |       10      |       32       | 60 sec. |  175 |  100  |    1%    |          Cubic          |



## Downloads

#### Binary

The final version of this of this software can be downloaded from subpage of this repository named **releases**. This project is already built for Linux, macOS and Windows operating systems, so building it on your own may be unnecessary.

#### Source code

You can download latest version of this tool source code using git. To do use git command:
> gic clone [TODO: ADD LINK HERE]

After downloading source code you can customize tool and build for your environment.



## Authors

This tool is developed by Jakub Biliński. You can find out more about me on my website: [jbilinski.pl](http:/www.jbilinski.pl).