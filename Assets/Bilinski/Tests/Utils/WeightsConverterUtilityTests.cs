﻿using Bilinski.Code.Utils;
using NUnit.Framework;

namespace Bilinski.Tests.Utils
{
    [TestFixture]
    class WeightsConverterUtilityTests
    {
        [Test]
        public void TwoWayTest()
        {
            var array = new float[2][,];
            array[0] = new[, ]{ { 1f, 2f, 0.25f }, { 0f, 0f, 2.199f } };
            array[1] = new[,] { { 0f, 12f, 0.7f }, { 0.99f, 0.2f, 3.14f } };
            var serializedArray = WeightsConverterUtility.Serialize(array);
            var deserializedArray = WeightsConverterUtility.Deserialize(serializedArray);
            Assert.AreEqual(array, deserializedArray);
        }
    }
}
