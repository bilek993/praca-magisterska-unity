﻿using System.Text.RegularExpressions;
using Bilinski.Code.Utils;
using NUnit.Framework;

namespace Bilinski.Tests.Utils
{
    [TestFixture]
    public class DateTimeUtilityTests
    {
        [Test]
        public void GetCurrentDateAndTimeTestWithRegex()
        {
            Assert.IsTrue(Regex.IsMatch(DateTimeUtility.GetCurrentDateAndTime(), @"\d\d\/\d\d\/\d\d\d\d \d\d:\d\d:\d\d"));
        }
    }
}
