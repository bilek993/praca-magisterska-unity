﻿using Bilinski.Code.AI;
using Bilinski.Code.AI.Functions;
using NUnit.Framework;

namespace Bilinski.Tests.AI
{
    [TestFixture]
    public class NeuralLayerTests
    {
        [Test]
        public void TestSetRandomWeights()
        {
            var neuralLayer1 = new NeuralLayer(5, 5, new LinearFunction());
            var neuralLayer2 = new NeuralLayer(5, 5, new LinearFunction());

            neuralLayer1.SetRandomWeights();
            neuralLayer2.SetRandomWeights();

            Assert.AreNotEqual(neuralLayer1.Weights, neuralLayer2.Weights);
        }

        [Test]
        public void TestFeedForwardOneOutputWeightsNotInitialized()
        {
            var layer = new NeuralLayer(5, 1, new LinearFunction());
            var output = layer.FeedForward(new[] {1f, 0f, 0.5f, 0f, 0.1f});

            Assert.AreEqual(output, new[] {0f});
        }

        [Test]
        public void TestFeedForwardWithPrecalculatedValues()
        {
            var layer = new NeuralLayer(5, 3, new LinearFunction());
            var weights = new float[5, 3];
            weights[0, 0] = 1f;
            weights[2, 1] = 1f;
            weights[4, 2] = 0.5f;
            layer.Weights = weights;

            var output = layer.FeedForward(new[] { 1f, 0f, 0.5f, 0f, 0.1f });

            Assert.AreEqual(output, new[] { 1f, 0.5f, 0.05f });
        }
    }
}
