﻿using System;
using System.Collections.Generic;
using Bilinski.Code.AI;
using Bilinski.Code.AI.Functions;
using NUnit.Framework;

namespace Bilinski.Tests.AI
{
    [TestFixture]
    public class NeuralNetworkTests
    {
        [Test]
        public void TestFeedForwadOnCorrectValues()
        {
            var network = new NeuralNetwork(new LinearFunction(), new List<int> {2, 3, 1});

            foreach (var layer in network.NeuralLayers)
            {
                layer.SetRandomWeights();
            }

            Assert.AreEqual(network.FeedForward(new[] { 0f, 0f }), new[] { 0f });
        }

        [Test]
        public void TestComparingNeuralNetworksEquals()
        {
            var firstNetwork75 = new NeuralNetwork(new LinearFunction())
            {
                Fitness = 75
            };

            var secondNetwork75 = new NeuralNetwork(new LinearFunction())
            {
                Fitness = 75
            };

            Assert.AreEqual(firstNetwork75.CompareTo(secondNetwork75), 0);
            Assert.AreNotSame(firstNetwork75, secondNetwork75);
        }

        [Test]
        public void TestComparingNeuralNetworksLessAndGreater()
        {
            var network5 = new NeuralNetwork(new LinearFunction())
            {
                Fitness = 5
            };

            var network9 = new NeuralNetwork(new LinearFunction())
            {
                Fitness = 9
            };

            Assert.AreEqual(network5.CompareTo(network9), -1);
            Assert.AreEqual(network9.CompareTo(network5), 1);
        }

        [Test]
        public void TestComparingNeuralNetworksNull()
        {
            var network = new NeuralNetwork(new LinearFunction());

            Assert.Throws<Exception>(() =>
            {
                var value = network.CompareTo(null);
            });
        }
    }
}
