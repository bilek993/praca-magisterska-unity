﻿using Bilinski.Code.AI.Functions;
using NUnit.Framework;

namespace Bilinski.Tests.AI.Functions
{
    [TestFixture()]
    public class TanhFunctionTests : ActivationFunctionTestBase
    {
        [OneTimeSetUp]
        public override void SetUpValues()
        {
            Function = new TanhFunction();
        }

        [TestCase(-0.5f, 2, -0.46f)] // http://www.wolframalpha.com/input/?i=NumberForm%5Btanh%5B-0.5%5D,2%5D
        [TestCase(0f, 2, 0f)] // http://www.wolframalpha.com/input/?i=NumberForm%5Btanh%5B0%5D,2%5D
        [TestCase(0.5f, 2, 0.46f)] // http://www.wolframalpha.com/input/?i=NumberForm%5Btanh%5B0.5%5D,2%5D
        public new void CalculateFunctionTest(float input, int precission, float expectedOutput)
        {
            base.CalculateFunctionTest(input, precission, expectedOutput);
        }

        [TestCase(-0.5f, 2, -0.46f)]
        [TestCase(0f, 2, 0f)]
        [TestCase(0.5f, 2, 0.46f)]
        public new void CalculateNormalizedFunctionTest(float input, int precission, float expectedOutput)
        {
            base.CalculateNormalizedFunctionTest(input, precission, expectedOutput);
        }

        [Test]
        public new void NormalizedFunctionOutOfRangeValuesTest()
        {
            base.NormalizedFunctionOutOfRangeValuesTest();
        }
    }
}
