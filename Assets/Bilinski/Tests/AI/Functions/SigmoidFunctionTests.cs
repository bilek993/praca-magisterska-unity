﻿using Bilinski.Code.AI.Functions;
using NUnit.Framework;

namespace Bilinski.Tests.AI.Functions
{
    [TestFixture]
    public class SigmoidFunctionTests : ActivationFunctionTestBase
    {
        [OneTimeSetUp]
        public override void SetUpValues()
        {
            Function = new SigmoidFunction();
        }

        [TestCase(-0.5f, 2, 0.38f)] // http://www.wolframalpha.com/input/?i=NumberForm%5BLogisticSigmoid%5B-0.5%5D,2%5D
        [TestCase(0f, 2, 0.5f)] // http://www.wolframalpha.com/input/?i=NumberForm%5BLogisticSigmoid%5B0%5D,2%5D
        [TestCase(0.5f, 2, 0.62f)] // http://www.wolframalpha.com/input/?i=NumberForm%5BLogisticSigmoid%5B0.5%5D,2%5D
        public new void CalculateFunctionTest(float input, int precission, float expectedOutput)
        {
            base.CalculateFunctionTest(input, precission, expectedOutput);
        }

        [TestCase(-0.5f, 2, -0.24f)] // http://www.wolframalpha.com/input/?i=NumberForm%5BLogisticSigmoid%5B-0.5%5D+*+2+-+1,2%5D
        [TestCase(0f, 2, 0f)] // http://www.wolframalpha.com/input/?i=NumberForm%5BLogisticSigmoid%5B0%5D+*+2+-+1,2%5D
        [TestCase(0.5f, 2, 0.24f)] // http://www.wolframalpha.com/input/?i=NumberForm%5BLogisticSigmoid%5B0.5%5D+*+2+-+1,2%5D
        public new void CalculateNormalizedFunctionTest(float input, int precission, float expectedOutput)
        {
            base.CalculateNormalizedFunctionTest(input, precission, expectedOutput);
        }

        [Test]
        public new void NormalizedFunctionOutOfRangeValuesTest()
        {
            base.NormalizedFunctionOutOfRangeValuesTest();
        }
    }
}
