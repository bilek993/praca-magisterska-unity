﻿using System;
using Bilinski.Code.AI.Functions;
using NUnit.Framework;

namespace Bilinski.Tests.AI.Functions
{
    public abstract class ActivationFunctionTestBase
    {
        private IActivationFunction _function;

        protected IActivationFunction Function
        {
            set { _function = value; }
        }

        public abstract void SetUpValues();

        protected void CalculateFunctionTest(float input, int precission, float expectedOutput)
        {
            Assert.AreEqual(expectedOutput, (float) Math.Round(_function.Calculate(input), precission));
        }

        protected void CalculateNormalizedFunctionTest(float input, int precission, float expectedOutput)
        {
            Assert.AreEqual(expectedOutput, (float)Math.Round(_function.CalculateAndNormalize(input), precission));
        }

        protected void NormalizedFunctionOutOfRangeValuesTest()
        {
            const float negativeValue = float.MinValue;
            const float positiveValue = float.MaxValue;

            Assert.GreaterOrEqual(_function.CalculateAndNormalize(negativeValue), -1f);
            Assert.GreaterOrEqual(_function.CalculateAndNormalize(positiveValue), 1f);
        }
    }
}
