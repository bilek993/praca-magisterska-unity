﻿using Bilinski.Code.AI.Functions;
using NUnit.Framework;

namespace Bilinski.Tests.AI.Functions
{
    [TestFixture]
    public class LinearFunctionTests : ActivationFunctionTestBase
    {
        [OneTimeSetUp]
        public override void SetUpValues()
        {
            Function = new LinearFunction();
        }

        [TestCase(-0.5f, 2, -0.5f)]
        [TestCase(0f, 2, 0f)]
        [TestCase(0.5f, 2, 0.5f)]
        public new void CalculateFunctionTest(float input, int precission, float expectedOutput)
        {
            base.CalculateFunctionTest(input, precission, expectedOutput);
        }

        [TestCase(-0.5f, 2, -0.5f)]
        [TestCase(0f, 2, 0f)]
        [TestCase(0.5f, 2, 0.5f)]
        public new void CalculateNormalizedFunctionTest(float input, int precission, float expectedOutput)
        {
            base.CalculateNormalizedFunctionTest(input, precission, expectedOutput);
        }

        [Test]
        public new void NormalizedFunctionOutOfRangeValuesTest()
        {
            base.NormalizedFunctionOutOfRangeValuesTest();
        }
    }
}
