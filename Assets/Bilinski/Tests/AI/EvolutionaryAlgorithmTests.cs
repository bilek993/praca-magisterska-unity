﻿using System;
using System.Collections.Generic;
using Bilinski.Code.AI;
using Bilinski.Code.AI.Functions;
using NUnit.Framework;
using UnityEngine;

namespace Bilinski.Tests.AI
{
    [TestFixture]
    public class EvolutionaryAlgorithmTests
    {
        [Test]
        public void CrossNetworksTest()
        {
            var network1 = new NeuralNetwork(new LinearFunction(), new List<int> { 5, 3 });
            var network2 = new NeuralNetwork(new LinearFunction(), new List<int> { 5, 3 });

            var network3 = EvolutionaryAlgorithm.CrossNetworks(network1, network2, new LinearFunction());

            Assert.AreNotEqual(network1.NeuralLayers[0].Weights, network3.NeuralLayers[0].Weights);
            Assert.AreNotEqual(network2.NeuralLayers[0].Weights, network3.NeuralLayers[0].Weights);
        }

        [Test]
        public void MutateNetworkTestMutationException()
        {
            Assert.Throws<Exception>(() =>
            {
                EvolutionaryAlgorithm.MutateNetwork(null, int.MaxValue);
            });

            Assert.Throws<Exception>(() =>
            {
                EvolutionaryAlgorithm.MutateNetwork(null, -1);
            });
        }

        [Test]
        public void MutateNetworkTest()
        {
            var network = new NeuralNetwork(new LinearFunction(), new List<int> { 5, 3 });
            var initialLayerWeights = (float[,])network.NeuralLayers[0].Weights.Clone();

            EvolutionaryAlgorithm.MutateNetwork(network, 99);
            Assert.AreNotEqual(initialLayerWeights, network.NeuralLayers[0].Weights);
        }

        [Test]
        public void RandomChildIdTest()
        {
            var neuralNetworks = new List<NeuralNetwork>();

            for (var i = 0; i < 100; i++)
            {
                neuralNetworks.Add(new NeuralNetwork(new LinearFunction())
                {
                    Fitness = i * 3
                });
            }

            for (var i = 0; i < 100; i++)
            {
                var randomValue = EvolutionaryAlgorithm.RandomChildId(neuralNetworks, ParentSelectionMethods.Tournament, 1);
                Assert.AreNotEqual(1, randomValue);

                randomValue = EvolutionaryAlgorithm.RandomChildId(neuralNetworks, ParentSelectionMethods.Random, 1);
                Assert.AreNotEqual(1, randomValue);

                randomValue = EvolutionaryAlgorithm.RandomChildId(neuralNetworks, ParentSelectionMethods.Cubic, 1);
                Assert.AreNotEqual(1, randomValue);

                randomValue = EvolutionaryAlgorithm.RandomChildId(neuralNetworks, ParentSelectionMethods.RouletteWheel, 1);
                Assert.AreNotEqual(1, randomValue);
            }
        }
    }
}
