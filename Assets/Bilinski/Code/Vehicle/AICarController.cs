﻿using System.Collections.Generic;
using Bilinski.Code.AI;
using Bilinski.Code.AI.Functions;
using Bilinski.Code.Camera;
using Bilinski.Code.Track;
using Bilinski.Code.UI;
using UnityEngine;

namespace Bilinski.Code.Vehicle
{
    public class AICarController : MonoBehaviour
    {
        public LayerMask RaycastLayerMask;
        public Transform[] Sennors;
        public Checkpoint NextCheckpoint;

        public NeuralNetwork Network { get; set; }

        private float _accelerationAndDeceleration;
        private float _steering;
        private float _currentSpeed;

        private CarCore _core;
        private SmoothCamera _smoothCamera;
        private ShakeCamera _shakeCamera;
        private VehicleGauges _vehicleHGauges;
        private BillboardSystem _billboardSystem;

        private float _maxRaycastDistance;
        private float _minVehicleSpeed;

        public float AccelerationAndDeceleration
        {
            get { return _accelerationAndDeceleration; }
        }

        public float Steering
        {
            get { return _steering; }
        }

        public float CurrentSpeed
        {
            get { return _currentSpeed; }
        }

        void Awake()
        {
            _core = GetComponent<CarCore>();
            _smoothCamera = UnityEngine.Camera.main.GetComponentInParent<SmoothCamera>();
            _shakeCamera = UnityEngine.Camera.main.GetComponent<ShakeCamera>();
        }

        void FixedUpdate()
        {
            if (Network != null)
            {
                FeedForward();
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            _shakeCamera.NotifyAboutCollision(gameObject, collision);
        }

        public void InitializeAI(List<int> neuronsOnLayers, IActivationFunction activationFunction, float maxRaycastDistance, 
            float minVehicleSpeed, Checkpoint initialCheckpoint, VehicleGauges gauges, BillboardSystem billboardSystem)
        {
            InitializeAI(neuronsOnLayers, activationFunction, maxRaycastDistance, minVehicleSpeed, initialCheckpoint,
                gauges, billboardSystem, new NeuralNetwork(activationFunction, neuronsOnLayers));
        }

        public void InitializeAI(List<int> neuronsOnLayers, IActivationFunction activationFunction, float maxRaycastDistance, 
            float minVehicleSpeed, Checkpoint initialCheckpoint, VehicleGauges gauges, BillboardSystem billboardSystem, NeuralNetwork neuralNetwork)
        {
            Network = neuralNetwork;
            _maxRaycastDistance = maxRaycastDistance;
            _minVehicleSpeed = minVehicleSpeed;
            NextCheckpoint = initialCheckpoint;
            _vehicleHGauges = gauges;
            _billboardSystem = billboardSystem;
        }

        private float[] GenerateSensorsData()
        {
            RaycastHit hit;
            var sensorsData = new float[Sennors.Length];

            for (var i = 0; i < Sennors.Length; i++)
            {
                var sensor = Sennors[i];
                var vector = sensor.transform.rotation.eulerAngles;
                sensor.rotation = Quaternion.Euler(0, vector.y, vector.z);

                if (Physics.Raycast(sensor.position, sensor.TransformDirection(Vector3.forward), out hit, _maxRaycastDistance, RaycastLayerMask))
                {
#if UNITY_EDITOR
                    Debug.DrawRay(sensor.position, sensor.TransformDirection(Vector3.forward) * hit.distance, Color.red);
#endif
                    sensorsData[i] = (hit.distance / _maxRaycastDistance) * -2  + 1;
                }
                else
                {
#if UNITY_EDITOR
                    Debug.DrawRay(sensor.position, sensor.TransformDirection(Vector3.forward) * _maxRaycastDistance, Color.green);
#endif
                    sensorsData[i] = -1;
                }
            }

            return sensorsData;
        }

        private void FeedForward()
        {
            var networkOutput = Network.FeedForward(GenerateSensorsData());
            _accelerationAndDeceleration = networkOutput[0];
            _steering = networkOutput[1];
            _currentSpeed = _core.CalculateVehicleSpeed();

            if (_currentSpeed <= _minVehicleSpeed)
            {
                _core.SetWheelsParameters(1, _steering, 0);
                _accelerationAndDeceleration = 1f;
            }
            else
            {
                if (_accelerationAndDeceleration >= 0)
                {
                    _core.SetWheelsParameters(_accelerationAndDeceleration, _steering, 0);
                }
                else
                {
                    if (_currentSpeed <= _minVehicleSpeed * 1.5)
                    {
                        _core.SetWheelsParameters(0, _steering, 0);
                    }
                    else
                    {
                        _core.SetWheelsParameters(0, _steering, -_accelerationAndDeceleration);
                    }
                }
            }
        }

        public void AddFitnessPoint(Checkpoint triggeredCheckpoint, Checkpoint nextCheckpoint)
        {
            if (triggeredCheckpoint != NextCheckpoint)
            {
                return;
            }

            Network.Fitness++;
            _smoothCamera.NotifyAboutNewPossibleTarget(transform, Network.Fitness);
            _shakeCamera.NotifyAboutNewPossibleTarget(gameObject, Network.Fitness);
            _vehicleHGauges.NotifyAboutNewPossibleTarget(this, Network.Fitness);
            _billboardSystem.NotifyAboutNewPossibleBestFitness(Network.Fitness);

            NextCheckpoint = nextCheckpoint;
        }
    }
}
