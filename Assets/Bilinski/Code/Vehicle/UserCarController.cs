﻿using UnityEngine;

namespace Bilinski.Code.Vehicle
{
    public class UserCarController : MonoBehaviour
    {
        private CarCore _core;

        private float _acceleration;
        private float _angel;
        private float _breaking;

        void Start()
        {
            _core = GetComponent<CarCore>();
            print("Only for testing purposes! Disable this script in production enviroment!!!");
        }

        void Update ()
        {
            if (_core == null)
            {
                print("CarCore not found!");
                return;
            }

            _acceleration = Input.GetKey(KeyCode.UpArrow) ? 1 : 0;
            _angel = Input.GetKey(KeyCode.LeftArrow) ? -1 : Input.GetKey(KeyCode.RightArrow) ? 1 : 0;
            _breaking = Input.GetKey(KeyCode.DownArrow) ? 1 : 0;

            _core.SetWheelsParameters(_acceleration, _angel, _breaking);
        }
    }
}
