﻿using UnityEngine;

namespace Bilinski.Code.Vehicle
{
    public class CarCore : MonoBehaviour
    {
        public WheelCollider WheelFL;
        public WheelCollider WheelFR;
        public WheelCollider WheelBL;
        public WheelCollider WheelBR;

        public Transform TireFL;
        public Transform TireFR;
        public Transform TireBL;
        public Transform TireBR;

        public GameObject ArrowMinimap;
        public GameObject CrossMinimap;

        public float MaxAccelerationForce;
        public float MaxAngel;
        public float MaxBreakingForce;
        public float MaxStoppedTime;
        public float StoppedSpeed;

        private float _acceleraion;
        private float _angel;
        private float _breaking;
        private float _vehicleStoppedTimeout;
        private Rigidbody _rigidbody;

        void Start()
        {
            _rigidbody = gameObject.GetComponent<Rigidbody>();
        }

        void FixedUpdate ()
        {
            UpdateTiresPositions();
            UpdateWheelsSpeedAndAngel();
            UpdateMinimap();
        }

        private void UpdateTiresPositions()
        {
            Quaternion quaternion;
            Vector3 postion;
        
            WheelFL.GetWorldPose(out postion, out quaternion);
            TireFL.position = postion;
            TireFL.rotation = quaternion;

            WheelFR.GetWorldPose(out postion, out quaternion);
            TireFR.position = postion;
            TireFR.rotation = quaternion;

            WheelBL.GetWorldPose(out postion, out quaternion);
            TireBL.position = postion;
            TireBL.rotation = quaternion;

            WheelBR.GetWorldPose(out postion, out quaternion);
            TireBR.position = postion;
            TireBR.rotation = quaternion;
        }

        private void UpdateWheelsSpeedAndAngel()
        {
            WheelBL.motorTorque = _acceleraion * MaxAccelerationForce;
            WheelBR.motorTorque = _acceleraion * MaxAccelerationForce;

            WheelFL.steerAngle = _angel * MaxAngel;
            WheelFR.steerAngle = _angel * MaxAngel;

            WheelFL.brakeTorque = _breaking * MaxBreakingForce;
            WheelFR.brakeTorque = _breaking * MaxBreakingForce;
            WheelBL.brakeTorque = _breaking * MaxBreakingForce;
            WheelBR.brakeTorque = _breaking * MaxBreakingForce;
        }

        private void UpdateMinimap()
        {
            if (CalculateVehicleSpeed() < StoppedSpeed)
            {
                _vehicleStoppedTimeout += Time.fixedDeltaTime;
            }
            else
            {
                _vehicleStoppedTimeout = 0;
            }

            var vehicleStopped = _vehicleStoppedTimeout >= MaxStoppedTime;
            ArrowMinimap.SetActive(!vehicleStopped);
            CrossMinimap.SetActive(vehicleStopped);
        }

        public void SetWheelsParameters(float acceleration, float angel, float breaking)
        {
            _acceleraion = Mathf.Clamp01(acceleration);
            _angel = Mathf.Clamp(angel, -1, 1);
            _breaking = Mathf.Clamp01(breaking);
        }

        public float CalculateVehicleSpeed()
        {
            return _rigidbody.velocity.magnitude * 3.6f;
        }
    }
}
