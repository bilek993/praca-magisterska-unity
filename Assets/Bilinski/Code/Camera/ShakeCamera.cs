﻿using UnityEngine;
using EZCameraShake;

namespace Bilinski.Code.Camera
{
    public class ShakeCamera : MonoBehaviour
    {
        public float MagnitudeMultiplier;
        public float CollisionDistanceultiplier;
        public float Roughness;
        public float FadeInTime;
        public float FadeOutTime;

        private GameObject _target;
        private int _bestTargetFitness;

        private CameraShaker _cameraShaker;

        void Start()
        {
            _cameraShaker = CameraShaker.Instance;
        }

        public void NotifyAboutCollision(GameObject callerGameObject, Collision collision)
        {
            if (_cameraShaker == null || _target == null)
            {
                return;
            }

            _cameraShaker.ShakeOnce(
                callerGameObject != _target
                    ? CalculateForce(collision, callerGameObject.transform)
                    : CalculateForce(collision), Roughness, FadeInTime,
                FadeOutTime);
        }

        public void NotifyAboutNewPossibleTarget(GameObject possibleTarget, int possibleTargetFitness)
        {
            if (possibleTargetFitness <= _bestTargetFitness)
            {
                return;
            }

            _target = possibleTarget;
            _bestTargetFitness = possibleTargetFitness;
        }

        public void Reset()
        {
            _target = null;
            _bestTargetFitness = 0;
        }

        private float CalculateForce(Collision collision)
        {
            return collision.relativeVelocity.magnitude * MagnitudeMultiplier;
        }

        private float CalculateForce(Collision collision, Transform callerTransform)
        {
            return (collision.relativeVelocity.magnitude * MagnitudeMultiplier) /
                   Mathf.Max(1f,
                       Vector3.Distance(_target.transform.position, callerTransform.position) *
                       CollisionDistanceultiplier);
        }
    }
}
