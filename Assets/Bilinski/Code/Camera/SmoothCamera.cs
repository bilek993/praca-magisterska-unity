﻿using UnityEngine;

namespace Bilinski.Code.Camera
{
    public class SmoothCamera : MonoBehaviour
    {
        public Vector3 Offset;
        public float CameraSpeed;

        private Transform _target;
        private int _bestTargetFitness = 0;
        private Vector3 _startPostation;

        void Start()
        {
            _startPostation = transform.position;
        }

        void Update()
        {
            if (_target != null)
            {
                transform.position = Vector3.Lerp(transform.position,
                    new Vector3(_target.position.x + Offset.x, Offset.y, _target.position.z + Offset.z),
                    Time.deltaTime * CameraSpeed);
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, _startPostation, Time.deltaTime * CameraSpeed);
                Reset();
            }
        }

        public void NotifyAboutNewPossibleTarget(Transform possibleTargetTransform, int possibleTargetFitness)
        {
            if (possibleTargetFitness <= _bestTargetFitness)
            {
                return;
            }

            _bestTargetFitness = possibleTargetFitness;
            _target = possibleTargetTransform;
        }

        public void Reset()
        {
            _target = null;
            _bestTargetFitness = 0;
        }
    }
}
