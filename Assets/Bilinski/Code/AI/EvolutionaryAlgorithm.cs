﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bilinski.Code.AI.Functions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Bilinski.Code.AI
{
    public static class EvolutionaryAlgorithm
    {
        public static NeuralNetwork CrossNetworks(NeuralNetwork parent1, NeuralNetwork parent2, IActivationFunction activationFunction)
        {
            var childNetwork = new NeuralNetwork(activationFunction);
            var layers = new NeuralLayer[parent1.NeuralLayers.Length];

            var parent1Layers = parent1.NeuralLayers;
            var parent2Layers = parent2.NeuralLayers;

            for (var i = 0; i < layers.Length; i++)
            {
                layers[i] = new NeuralLayer(parent1Layers[i].Weights.GetLength(0),
                    parent1Layers[i].Weights.GetLength(1), activationFunction);

                for (var x = 0; x < layers[i].Weights.GetLength(0); x++)
                {
                    for (var y = 0; y < layers[i].Weights.GetLength(1); y++)
                    {
                        if (Random.Range(0, 99) >= 50)
                        {
                            layers[i].Weights[x, y] = parent1Layers[i].Weights[x,y];
                        }
                        else
                        {
                            layers[i].Weights[x, y] = parent2Layers[i].Weights[x, y];
                        }
                    }
                }
            }

            childNetwork.NeuralLayers = layers;
            return childNetwork;
        }

        public static void MutateNetwork(NeuralNetwork networkForMutation, int mutationChance)
        {
            if (mutationChance > 100 || mutationChance < 0)
            {
                throw new Exception("Mutation chance is out of range");
            }

            foreach (var layer in networkForMutation.NeuralLayers)
            {
                for (var x = 0; x < layer.Weights.GetLength(0); x++)
                {
                    for (var y = 0; y < layer.Weights.GetLength(1); y++)
                    {
                        if (Random.Range(1, 100) > mutationChance)
                        {
                            continue;
                        }

                        var mutationType = Random.Range(0, 4);
                        switch (mutationType)
                        {
                            case 0:
                                layer.Weights[x, y] *= -1;
                                break;
                            case 1:
                                layer.Weights[x, y] = Random.Range(-1f, 1f);
                                break;
                            case 2:
                                layer.Weights[x, y] *= Random.Range(1f, 2f);
                                break;
                            case 3:
                                layer.Weights[x, y] *= Random.Range(0f, 1f);
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }
            }
        }

        public static int RandomChildId(List<NeuralNetwork> networsList, ParentSelectionMethods parentSelectionMethod, int skipId = -1)
        {
            switch (parentSelectionMethod)
            {
                case ParentSelectionMethods.Random:
                    return SkipIdIfNeeded(RandomSelection(networsList.Count), networsList.Count, skipId);
                case ParentSelectionMethods.Cubic:
                    return SkipIdIfNeeded(CubicSelection(networsList.Count), networsList.Count, skipId);
                case ParentSelectionMethods.RouletteWheel:
                    return SkipIdIfNeeded(RouletteWheelSelection(networsList), networsList.Count, skipId);
                case ParentSelectionMethods.Tournament:
                    return SkipIdIfNeeded(TournamentSelection(networsList), networsList.Count, skipId);
                default:
                    return SkipIdIfNeeded(TournamentSelection(networsList), networsList.Count, skipId);
            }
        }

        private static int RandomSelection(int childCount)
        {
            return Random.Range(0, childCount);
        }

        private static int CubicSelection(int childCount)
        {
            var randomValue = Random.Range(0f, 1f);
            randomValue = Mathf.Pow(randomValue, 3) * childCount / 2f;
            return (int)Mathf.Round(randomValue);
        }

        private static int RouletteWheelSelection(List<NeuralNetwork> networkList)
        {
            var fitnessSum = networkList.Aggregate(0f, (current, network) => current + network.Fitness);
            var randomValue = Random.Range(0, fitnessSum);

            var partialSum = 0f;
            var networkPosition = -1;

            while (randomValue > partialSum)
            {
                partialSum += networkList[++networkPosition].Fitness;
            }

            return networkPosition;
        }

        private static int TournamentSelection(List<NeuralNetwork> networsList)
        {
            var bestId = 0;
            var bestFitness = 0;

            for (var i = 0; i < networsList.Count % 10; i++)
            {
                var randomId = Random.Range(0, networsList.Count);

                if (bestFitness < networsList[randomId].Fitness)
                {
                    bestId = randomId;
                    bestFitness = networsList[randomId].Fitness;
                }
            }

            return bestId;
        }

        private static int SkipIdIfNeeded(int currentId, int networkSize, int skipId)
        {
            if (currentId == skipId)
            {
                if (currentId > 0)
                {
                    return currentId - 1;
                }
                else if (currentId < networkSize - 1)
                {
                    return currentId + 1;
                }
            }

            return currentId;
        }
    }
}