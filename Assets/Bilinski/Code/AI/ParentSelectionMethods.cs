﻿namespace Bilinski.Code.AI
{
    public enum ParentSelectionMethods
    {
        Random,
        Cubic,
        RouletteWheel,
        Tournament
    }
}
