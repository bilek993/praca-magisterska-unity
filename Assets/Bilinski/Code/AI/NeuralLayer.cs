﻿using Bilinski.Code.AI.Functions;
using UnityEngine;

namespace Bilinski.Code.AI
{
    public class NeuralLayer
    {
        public float[,] Weights { get; set; }

        private readonly IActivationFunction _activationFunction;

        public NeuralLayer(int neuronsCount, int outputCount, IActivationFunction activationFunction)
        {
            Weights = new float[neuronsCount, outputCount];
            _activationFunction = activationFunction;
        }

        public void SetRandomWeights()
        {
            for (var i = 0; i < Weights.GetLength(0); i++)
            {
                for (var j = 0; j < Weights.GetLength(1); j++)
                {
                    Weights[i, j] = Random.Range(-1f, 1f);
                }
            }
        }

        public float[] FeedForward(float[] inputs, bool useNormalizedFunction = false)
        {
            var sums = new float[Weights.GetLength(1)];

            for (var i = 0; i < Weights.GetLength(0); i++)
            {
                for (var j = 0; j < Weights.GetLength(1); j++)
                {
                    sums[j] += Weights[i, j] * inputs[i];
                }
            }

            for (var j = 0; j < Weights.GetLength(1); j++)
            {
                if (useNormalizedFunction)
                {
                    sums[j] = _activationFunction.CalculateAndNormalize(sums[j]);
                }
                else
                {
                    sums[j] = _activationFunction.Calculate(sums[j]);
                }
            }


            return sums;
        }
    }
}
