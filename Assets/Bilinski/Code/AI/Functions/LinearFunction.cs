﻿using UnityEngine;

namespace Bilinski.Code.AI.Functions
{
    public class LinearFunction : IActivationFunction
    {
        public float Calculate(float input)
        {
            return input;
        }

        public float CalculateAndNormalize(float input)
        {
            return Mathf.Clamp(Calculate(input), -1, 1);
        }
    }
}
