﻿namespace Bilinski.Code.AI.Functions
{
    public interface IActivationFunction
    {
        float Calculate(float input);
        float CalculateAndNormalize(float input);
    }
}
