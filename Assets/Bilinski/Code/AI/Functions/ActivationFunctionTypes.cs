﻿namespace Bilinski.Code.AI.Functions
{
    public enum ActivationFunctionTypes
    {
        Linear,
        Sigmoid,
        Signum,
        Tanh
    }
}