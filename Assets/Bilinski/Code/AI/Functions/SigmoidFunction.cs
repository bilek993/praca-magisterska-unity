﻿using UnityEngine;

namespace Bilinski.Code.AI.Functions
{
    public class SigmoidFunction : IActivationFunction
    {
        public float Calculate(float input)
        {
            return 1.0f / (1.0f + Mathf.Exp(-input));
        }

        public float CalculateAndNormalize(float input)
        {
            return Mathf.Clamp((Calculate(input) * 2) - 1, -1, 1);
        }
    }
}
