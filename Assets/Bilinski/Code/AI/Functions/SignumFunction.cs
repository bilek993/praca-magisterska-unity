﻿namespace Bilinski.Code.AI.Functions
{
    public class SignumFunction : IActivationFunction
    {
        public float Calculate(float input)
        {
            if (input > 0)
            {
                return 1;
            }
            else if (input < 0)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        public float CalculateAndNormalize(float input)
        {
            return Calculate(input);
        }
    }
}
