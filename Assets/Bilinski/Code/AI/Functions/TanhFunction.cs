﻿using System;
using UnityEngine;

namespace Bilinski.Code.AI.Functions
{
    public class TanhFunction : IActivationFunction
    {
        public float Calculate(float input)
        {
            return (float)Math.Tanh(input);
        }

        public float CalculateAndNormalize(float input)
        {
            return Mathf.Clamp(Calculate(input), -1, 1);
        }
    }
}
