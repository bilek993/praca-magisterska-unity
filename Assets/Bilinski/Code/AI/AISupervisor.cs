﻿using System.Collections.Generic;
using Bilinski.Code.AI.Functions;
using Bilinski.Code.Camera;
using Bilinski.Code.Track;
using Bilinski.Code.UI;
using Bilinski.Code.Utils;
using Bilinski.Code.Vehicle;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Bilinski.Code.AI
{
    public class AISupervisor : MonoBehaviour
    {
        public GameObject CarPrefab;
        public Checkpoint FirstCheckpoint;
        public VehicleGauges Gauges;
        public BillboardSystem SystemOfBillboards;
        public TestingSceneTransitionManager TransitionManager;
        public SimulationSpeedSettings SpeedSettings;

        private LearningSettings _settings;
        private IActivationFunction _activationFunction;
        private float _timeOut;
        private int _currentGeneration;
        private int _bestFitness;
        private SmoothCamera _smoothCamera;
        private ShakeCamera _shakeCamera;
        private ResultTranferObject _resultTranferObject;
        private bool _testsFinished;

        private List<AICarController> _carsList;

        void Start ()
        {
            _smoothCamera = UnityEngine.Camera.main.GetComponentInParent<SmoothCamera>();
            _shakeCamera = UnityEngine.Camera.main.GetComponent<ShakeCamera>();
            _settings = LearningSettings.Instance;

            _resultTranferObject = ResultTranferObject.Instance;
            _resultTranferObject.SimulationStartDate = DateTimeUtility.GetCurrentDateAndTime();
            _resultTranferObject.Settings = _settings;
            _resultTranferObject.BestFitnessHistory = new List<int>();

            if (!_settings.InitializedFromMainMenu)
            {
                SetDebugValues();
            }

            _activationFunction = CreateActivationFunction(_settings.SelectedActivationFunction);
            AddRequiredLayers();

            _timeOut = _settings.Timeout;
            _currentGeneration = 1;

            _carsList = new List<AICarController>();
            for (var i = 0; i < _settings.GenerationSize; i++)
            {
                var initizalizedCar = Instantiate(CarPrefab);
                initizalizedCar.GetComponent<AICarController>().InitializeAI(_settings.NeuronsOnLayers,
                    _activationFunction, _settings.SensorsLength, _settings.MinSpeed, FirstCheckpoint, Gauges, SystemOfBillboards);
                _carsList.Add(initizalizedCar.GetComponent<AICarController>());
            }
        }

        void FixedUpdate()
        {
            _timeOut -= Time.fixedDeltaTime;

            if (_timeOut <= 0 && !_testsFinished)
            {
                _timeOut = _settings.Timeout;
                SystemOfBillboards.ResetFitness();

                if (++_currentGeneration > _settings.GenerationCount)
                {
                    _testsFinished = true;
                    FinishTests();
                    return;
                }

                CreateNextGeneartion();

                _smoothCamera.Reset();
                _shakeCamera.Reset();
                Gauges.Reset();
            }

            SystemOfBillboards.UpdateBasicData(_timeOut, _currentGeneration);
        }

        public void SkipGeneration()
        {
            _timeOut = 0f;
        }

        private void FinishTests()
        {
            var networksList = GenerateSortedList();

            _resultTranferObject.BestFitnessHistory.Add(networksList[0].Fitness);
            _resultTranferObject.SimulationEndDate = DateTimeUtility.GetCurrentDateAndTime();

            SpeedSettings.ResetSpeed();

            StartCoroutine(TransitionManager.ShowResultsScene());
        }

        private void CreateNextGeneartion()
        {
            var networksList = GenerateSortedList();
            var currentGenerationBestNetwork = networksList[0];

            _resultTranferObject.BestFitnessHistory.Add(currentGenerationBestNetwork.Fitness);

            if (currentGenerationBestNetwork.Fitness >= _bestFitness)
            {
                _bestFitness = currentGenerationBestNetwork.Fitness;
                var bestWeights = new float[currentGenerationBestNetwork.NeuralLayers.Length][,];

                for (var i = 0; i < bestWeights.Length; i++)
                {
                    bestWeights[i] = currentGenerationBestNetwork.NeuralLayers[i].Weights;
                }

                _resultTranferObject.BestWeights = WeightsConverterUtility.Serialize(bestWeights);
            }

            _carsList = new List<AICarController>();
            for (var i = 0; i < _settings.GenerationSize; i++)
            {
                var firstParentId = EvolutionaryAlgorithm.RandomChildId(networksList, _settings.ParentSelectionMethod);
                var secondParentId =
                    EvolutionaryAlgorithm.RandomChildId(networksList, _settings.ParentSelectionMethod, firstParentId);

                var childNeuralNetwork = EvolutionaryAlgorithm.CrossNetworks(networksList[firstParentId],
                    networksList[secondParentId], _activationFunction);
                EvolutionaryAlgorithm.MutateNetwork(childNeuralNetwork, _settings.MutationChance);

                var initizalizedCar = Instantiate(CarPrefab);
                initizalizedCar.GetComponent<AICarController>().InitializeAI(_settings.NeuronsOnLayers,
                    _activationFunction, _settings.SensorsLength, _settings.MinSpeed, FirstCheckpoint, Gauges, 
                    SystemOfBillboards, childNeuralNetwork);
                _carsList.Add(initizalizedCar.GetComponent<AICarController>());
            }
        }

        private List<NeuralNetwork> GenerateSortedList()
        {
            var networksList = new List<NeuralNetwork>();

            foreach (var controller in _carsList)
            {
                networksList.Add(controller.Network);
                Destroy(controller.gameObject);
            }

            networksList.Sort();
            networksList.Reverse();

            return networksList;
        }

        private void SetDebugValues()
        {
            Debug.Log("LearningSettings not initialzied from main menu!");
            Debug.Log("Values for testing scene has been set to debug values.");

            _settings.SelectedActivationFunction = ActivationFunctionTypes.Tanh;
            _settings.NeuronsOnLayers = new List<int> { 10, 5 };
            _settings.MinSpeed = 10;
            _settings.SensorsLength = 35;
            _settings.GenerationSize = 100;
            _settings.GenerationCount = 10;
            _settings.Timeout = 60;
            _settings.MutationChance = 1;
            _settings.ParentSelectionMethod = ParentSelectionMethods.Tournament;
        }

        private IActivationFunction CreateActivationFunction(ActivationFunctionTypes type)
        {
            switch (type)
            {
                case ActivationFunctionTypes.Linear:
                    return new LinearFunction();
                case ActivationFunctionTypes.Sigmoid:
                    return new SigmoidFunction();
                case ActivationFunctionTypes.Signum:
                    return new SignumFunction();
                case ActivationFunctionTypes.Tanh:
                    return new TanhFunction();
                default:
                    return new LinearFunction();
            }
        }

        private void AddRequiredLayers()
        {
            _settings.NeuronsOnLayers.Add(2);
            _settings.NeuronsOnLayers.Insert(0, CarPrefab.GetComponent<AICarController>().Sennors.Length);
        }
    }
}
