﻿using System;
using System.Collections.Generic;
using Bilinski.Code.AI.Functions;

namespace Bilinski.Code.AI
{
    [Serializable]
    public class LearningSettings
    {
        private static LearningSettings _instance;

        public static LearningSettings Instance
        {
            get { return _instance ?? (_instance = new LearningSettings()); }
        }

        // No getters or setters due to serialization
        public ActivationFunctionTypes SelectedActivationFunction;
        public List<int> NeuronsOnLayers;
        public int SensorsLength;
        public int MinSpeed;
        public int GenerationSize;
        public int GenerationCount;
        public int Timeout;
        public int MutationChance;
        public ParentSelectionMethods ParentSelectionMethod;

        public bool InitializedFromMainMenu { get; set; }

        private LearningSettings() {} // Singleton constructor
    }
}
