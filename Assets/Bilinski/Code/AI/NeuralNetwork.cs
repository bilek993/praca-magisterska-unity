﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bilinski.Code.AI.Functions;

namespace Bilinski.Code.AI
{
    public class NeuralNetwork : IComparable<NeuralNetwork>
    {
        public int Fitness { get; set; }
        public NeuralLayer[] NeuralLayers { get; set; }

        private readonly IActivationFunction _activationFunction;

        public NeuralNetwork(IActivationFunction activationFunction, List<int> neuralNetworkSize)
        {
            Fitness = 0;

            _activationFunction = activationFunction;
            NeuralLayers = new NeuralLayer[neuralNetworkSize.Count - 1];

            for (var i = 0; i < NeuralLayers.Length; i++)
            {
                NeuralLayers[i] = new NeuralLayer(neuralNetworkSize[i], neuralNetworkSize[i + 1], activationFunction);
                NeuralLayers[i].SetRandomWeights();
            }
        }

        public NeuralNetwork(IActivationFunction activationFunction)
        {
            Fitness = 0;

            _activationFunction = activationFunction;
        }

        public float[] FeedForward(float[] input)
        {
            var sum = input;

            for (var i = 0; i < NeuralLayers.Length - 1; i++)
            {
                sum = NeuralLayers[i].FeedForward(sum);
            }

            sum = NeuralLayers[NeuralLayers.Length - 1].FeedForward(sum, true);

            return sum;
        }

        public int CompareTo(NeuralNetwork other)
        {
            if (other == null)
            {
                throw new Exception("Cannot compare NeuralNetwork to null.");
            }

            if (other.Fitness > Fitness)
            {
                return -1;
            }
            else if (other.Fitness < Fitness)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
