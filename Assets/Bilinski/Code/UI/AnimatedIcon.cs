﻿using UnityEngine;
using UnityEngine.UI;

namespace Bilinski.Code.UI
{
    public class AnimatedIcon : MonoBehaviour
    {
        public Color DisabledColor;
        public Color EnabledColor;
        public float AnimationSpeed;

        private Image _iconImage;
        private bool _visible;

        void Start()
        {
            _iconImage = GetComponent<Image>();
        }

        void FixedUpdate()
        {
            if (_visible)
            {
                _iconImage.color = Color.Lerp(_iconImage.color, EnabledColor, Time.deltaTime * AnimationSpeed);
            }
            else
            {
                _iconImage.color = Color.Lerp(_iconImage.color, DisabledColor, Time.deltaTime * AnimationSpeed);
            }
        }

        public void Show()
        {
            _visible = true;
        }

        public void Hide()
        {
            _visible = false;
        }
    }
}
