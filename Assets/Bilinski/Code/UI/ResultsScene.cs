﻿using System.Collections;
using System.IO;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Bilinski.Code.UI
{
    public class ResultsScene : MonoBehaviour
    {
        public GameObject Title;
        public Transform TittleTarget;
        public GameObject[] ButtonsForActivation;

        IEnumerator Start ()
        {
            yield return new WaitForSeconds(1f);
            iTween.ScaleTo(Title, iTween.Hash("scale", new Vector3(0.75f, 0.75f, 0.75f), "easeType", "easeInOutQuart", "time", 0.75));
            iTween.MoveTo(Title, iTween.Hash("y", TittleTarget.position.y, "easeType", "easeInOutQuart", "time", 0.75));
            yield return new WaitForSeconds(0.6f);
            ActivateButtons();
        }

        private void ActivateButtons()
        {
            foreach (var button in ButtonsForActivation)
            {
                button.SetActive(true);
            }
        }

        public void FinishApplication()
        {
            Application.Quit();
        }

        public void SaveResultsToFile()
        {
            string fileName;
            do
            {
                fileName = GenerateNewFileName();
            } while (File.Exists(fileName));

            var file = File.CreateText(fileName);
            file.WriteLine(JsonUtility.ToJson(ResultTranferObject.Instance));
            file.Close();

            Application.OpenURL("file://" + Application.persistentDataPath);
        }

        private string GenerateNewFileName()
        {
            var randomValue = Random.Range(10000, 99999);
            return Application.persistentDataPath + "/result" + randomValue + ".json";
        }
    }
}
