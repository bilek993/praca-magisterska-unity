﻿using Bilinski.Code.Vehicle;
using UnityEngine;
using UnityEngine.UI;

namespace Bilinski.Code.UI
{
    public class VehicleGauges : MonoBehaviour
    {
        public Image AccelerationImage;
        public Image DecelerationImage;
        public RectTransform SteeringTransform;
        public Image SpeedImage;
        public Text SpeedText;

        public float MaxSpeedOnGauge;
        public float LerpSpeed;

        private AICarController _controller;
        private int _bestFitness = 0;

        void FixedUpdate ()
        {
            if (_controller != null)
            {
                if (_controller.AccelerationAndDeceleration > 0)
                {
                    AccelerationImage.fillAmount = Mathf.Lerp(AccelerationImage.fillAmount,
                        _controller.AccelerationAndDeceleration, Time.fixedDeltaTime * LerpSpeed);
                    DecelerationImage.fillAmount = Mathf.Lerp(DecelerationImage.fillAmount,
                        0, Time.fixedDeltaTime * LerpSpeed);
                }
                else
                {
                    AccelerationImage.fillAmount = Mathf.Lerp(AccelerationImage.fillAmount,
                        0, Time.fixedDeltaTime * LerpSpeed);
                    DecelerationImage.fillAmount = Mathf.Lerp(DecelerationImage.fillAmount,
                        -_controller.AccelerationAndDeceleration, Time.fixedDeltaTime * LerpSpeed);
                }

                SteeringTransform.rotation = Quaternion.Euler(new Vector3(0, 0,
                    Mathf.LerpAngle(SteeringTransform.rotation.eulerAngles.z, -_controller.Steering * 90 + 90,
                        Time.fixedDeltaTime * LerpSpeed)));

                SpeedText.text = Mathf.RoundToInt(_controller.CurrentSpeed).ToString();
                if (MaxSpeedOnGauge >= _controller.CurrentSpeed)
                {
                    SpeedImage.fillAmount = _controller.CurrentSpeed / MaxSpeedOnGauge;
                }
                else
                {
                    SpeedImage.fillAmount = 1f;
                }
            }
            else
            {
                Reset();
            }
        }

        public void NotifyAboutNewPossibleTarget(AICarController possibleTargetAICarController, int fitness)
        {
            if (fitness > _bestFitness)
            {
                _bestFitness = fitness;
                _controller = possibleTargetAICarController;
            }
        }

        public void Reset()
        {
            _controller = null;
            _bestFitness = 0;
        }
    }
}
