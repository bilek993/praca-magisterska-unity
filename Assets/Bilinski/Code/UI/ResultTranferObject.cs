﻿using System;
using System.Collections.Generic;
using Bilinski.Code.AI;

namespace Bilinski.Code.UI
{
    [Serializable]
    public class ResultTranferObject
    {
        private static ResultTranferObject _instance;

        public static ResultTranferObject Instance 
        {
            get { return _instance ?? (_instance = new ResultTranferObject()); }
        }

        public string SimulationStartDate;
        public string SimulationEndDate;
        public LearningSettings Settings;
        public List<int> BestFitnessHistory;
        public string BestWeights;

        private ResultTranferObject() {} // Singleton constructor
    }
}
