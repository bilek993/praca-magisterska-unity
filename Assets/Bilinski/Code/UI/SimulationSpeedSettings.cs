﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Bilinski.Code.UI
{
    [RequireComponent(typeof(Text))]
    public class SimulationSpeedSettings : MonoBehaviour
    {
        public float Step;
        public float MinimalValue;
        public float MaxValue;

        private Text _text;
        private float _visibilityTimer;
        private float _simulationCurrentSpeed;

        void Start ()
        {
            _text = GetComponent<Text>();
	    
            ResetSpeed();
        }
	
        void Update ()
        {
            if (Input.GetKeyDown(KeyCode.Minus) || Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                SetSpeed(-Step);
            }

            if (Input.GetKeyDown(KeyCode.Plus) || Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                SetSpeed(Step);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ResetSpeed();
            }
        }

        void FixedUpdate()
        {
            if (_visibilityTimer > 0)
            {
                _visibilityTimer -= Time.unscaledDeltaTime;
                _text.text = String.Format("{0:0.0}x Speed", _simulationCurrentSpeed); ;
            }
            else
            {
                _text.text = "";
            }
        }

        public void ResetSpeed()
        {
            _simulationCurrentSpeed = 1f;
            Time.timeScale = _simulationCurrentSpeed;
            _visibilityTimer = 0f;
        }

        private void SetSpeed(float addedValue)
        {
            if (_simulationCurrentSpeed + addedValue > MaxValue || _simulationCurrentSpeed + addedValue < MinimalValue)
            {
                return;
            }

            _simulationCurrentSpeed += addedValue;
            Time.timeScale = _simulationCurrentSpeed;

            _visibilityTimer = 3f;
        }
    }
}
