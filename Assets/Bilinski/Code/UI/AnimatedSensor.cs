﻿using UnityEngine;
using UnityEngine.UI;

namespace Bilinski.Code.UI
{
    public class AnimatedSensor : MonoBehaviour
    {
        public float SensorLength;
        public float AnimationSpeed;

        private Image _sensorImage;

        void Start ()
        {
            _sensorImage = gameObject.GetComponent<Image>();
        }
	
        void FixedUpdate ()
        {
            _sensorImage.fillAmount = Mathf.Lerp(_sensorImage.fillAmount, SensorLength, Time.fixedDeltaTime * AnimationSpeed);
        }
    }
}
