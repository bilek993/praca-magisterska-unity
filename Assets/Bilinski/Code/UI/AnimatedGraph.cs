﻿using Bilinski.Code.AI.Functions;
using UnityEngine;

namespace Bilinski.Code.UI
{
    [RequireComponent(typeof(LineRenderer))]
    public class AnimatedGraph : MonoBehaviour
    {
        public float GraphAnimationSpeed;

        private LineRenderer _lineRenderer;
        private IActivationFunction _activationFunction;

        void Start ()
        {
            _lineRenderer = GetComponent<LineRenderer>();

            for (var i = 0; i < _lineRenderer.positionCount; i++)
            {
                _lineRenderer.SetPosition(i, new Vector3((float)(i - 100) / 100,0,0));
            }
        }
	
        void FixedUpdate ()
        {
            if (_activationFunction == null)
            {
                return;
            }

            for (var i = 0; i < _lineRenderer.positionCount; i++)
            {
                var x = (float) (i - 100) / 100;
                var y = Mathf.Lerp(_lineRenderer.GetPosition(i).y, _activationFunction.CalculateAndNormalize(x*2) / 2, Time.fixedDeltaTime * GraphAnimationSpeed);
                _lineRenderer.SetPosition(i, new Vector3(x, y, 0));
            }
        }

        public void SetActivationFunctionAsLinear()
        {
            _activationFunction = new LinearFunction();
        }

        public void SetActivationFunctionAsSignum()
        {
            _activationFunction = new SignumFunction();
        }

        public void SetActivationFunctionAsSigmoid()
        {
            _activationFunction = new SigmoidFunction();
        }

        public void SetActivationFunctionAsTanh()
        {
            _activationFunction = new TanhFunction();
        }
    }
}
