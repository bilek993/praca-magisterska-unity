﻿using UnityEngine;
using UnityEngine.UI;

namespace Bilinski.Code.UI
{
    public class SimpleExitMenu : MonoBehaviour
    {
        public GameObject[] MenuItems;
        public float Timeout;
        public float BlurTimeout;
        public Image TimeoutIndicator;
        public Material BlurMaterial;

        private float _timerValue;
	
        void FixedUpdate ()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                _timerValue += Time.fixedDeltaTime;
                SetMenuVisibility(true);
                TimeoutIndicator.fillAmount = _timerValue / Timeout;

                if (_timerValue / BlurTimeout < 1f)
                {
                    BlurMaterial.SetFloat("_EffectScale", _timerValue / BlurTimeout);
                }
                else
                {
                    BlurMaterial.SetFloat("_EffectScale", 1f);
                }

                if (_timerValue > Timeout)
                {
                    Application.Quit();
                }
            }
            else if (_timerValue > 0f)
            {
                _timerValue = 0;
                BlurMaterial.SetFloat("_EffectScale", 0f);
                SetMenuVisibility(false);
            }
        }

        private void SetMenuVisibility(bool visible)
        {
            foreach (var menu in MenuItems)
            {
                menu.SetActive(visible);
            }
        }
    }
}
