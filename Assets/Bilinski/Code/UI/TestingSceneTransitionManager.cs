﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Bilinski.Code.UI
{
    public class TestingSceneTransitionManager : MonoBehaviour
    {
        public float TranstionTime;
        public Image TransitionImage;
        public GameObject MainUiCanvas;

        IEnumerator Start()
        {
            TransitionImage.CrossFadeAlpha(0f, TranstionTime, false);
            yield return new WaitForSeconds(TranstionTime);
            TransitionImage.gameObject.SetActive(false);
            MainUiCanvas.SetActive(true);
        }

        public IEnumerator ShowResultsScene()
        {
            MainUiCanvas.SetActive(false);
            TransitionImage.gameObject.SetActive(true);
            TransitionImage.CrossFadeAlpha(0f, 0f, false);
            TransitionImage.CrossFadeAlpha(1f, TranstionTime, false);
            yield return new WaitForSeconds(TranstionTime);

            SceneManager.LoadScene("Results");
        }
    }
}
