﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bilinski.Code.AI;
using Bilinski.Code.AI.Functions;
using SFB;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Bilinski.Code.UI
{
    public class MainMenu : MonoBehaviour
    {
        public GameObject BeginScreen;
        public GameObject Settings1;
        public GameObject Settings2;
        public GameObject Settings3;
        public GameObject Settings4;
        public GameObject Settings5;

        public CustomDropdown ActivationFunctionCustomDropdown;
        public CustomDropdown ParentSelectionCustomDropdown;

        public RadialSlider[] LayerSliders;
        public RadialSlider MinSpeedRadialSlider;
        public RadialSlider SensorsLengthRadialSlider;
        public RadialSlider GenerationSizeSlider;
        public RadialSlider GenerationCountRadialSlider;
        public RadialSlider TimeoutRadialSlider;
        public RadialSlider MutationChanceSlider;

        public GameObject TopDownVehicle;
        public AnimatedSensor[] AnimatedSensors;
        public Transform TargetTopDownVehicle;

        public Image TransitionImage;
        public float TranstionTime;

        public float RotationEmptyView;

        IEnumerator Start()
        {
            TransitionImage.CrossFadeAlpha(0f, TranstionTime, false);
            yield return new WaitForSeconds(TranstionTime);
            Destroy(TransitionImage.gameObject);
        }

        public void OnButtonStartClick()
        {
            BeginScreen.SetActive(false);
            iTween.RotateTo(gameObject, iTween.Hash("y", RotationEmptyView, "easeType", "easeInOutBack"));
            StartCoroutine(ShowSettings1Screen());
        }

        private IEnumerator ShowSettings1Screen()
        {
            yield return new WaitForSeconds(1f);
            Settings1.SetActive(true);
        }

        public void UpdateAnimatedSensors()
        {
            foreach (var sensor in AnimatedSensors)
            {
                sensor.SensorLength = SensorsLengthRadialSlider.currentValue / SensorsLengthRadialSlider.maxValue;
            }
        }

        public void ShowSettings2Screen()
        {
            Settings1.SetActive(false);
            Settings2.SetActive(true);
        }

        public void ShowSettings3Screen()
        {
            Settings2.SetActive(false);
            Settings3.SetActive(true);
            iTween.MoveTo(TopDownVehicle, iTween.Hash("x", TargetTopDownVehicle.position.x, "easeType", "easeOutSine", "time", 0.75));
        }

        public void ShowSettings4Screen()
        {
            Settings3.SetActive(false);
            Settings4.SetActive(true);
        }

        public void ShowSettings5Screen()
        {
            Settings4.SetActive(false);
            Settings5.SetActive(true);
        }

        public void RunSimulation()
        {
            Settings5.SetActive(false);

            var settings = LearningSettings.Instance;
            settings.SelectedActivationFunction =
                FunctionTypeFromString(ActivationFunctionCustomDropdown.selectedText.text);
            settings.NeuronsOnLayers = CreateListOfLayers();
            settings.SensorsLength = Mathf.FloorToInt(SensorsLengthRadialSlider.currentValue);
            settings.MinSpeed = Mathf.FloorToInt(MinSpeedRadialSlider.currentValue);
            settings.GenerationSize = Mathf.FloorToInt(GenerationSizeSlider.currentValue);
            settings.GenerationCount = Mathf.FloorToInt(GenerationCountRadialSlider.currentValue);
            settings.Timeout = Mathf.FloorToInt(TimeoutRadialSlider.currentValue);
            settings.MutationChance = Mathf.FloorToInt(MutationChanceSlider.currentValue);
            settings.ParentSelectionMethod =
                ParentSelectionMethodFromString(ParentSelectionCustomDropdown.selectedText.text);
            settings.InitializedFromMainMenu = true;

            SceneManager.LoadScene("Testing");
        }

        public void RestoreSimulation()
        {
            var paths = StandaloneFileBrowser.OpenFilePanel("Select result file", Application.persistentDataPath, "json", false);

            if (paths.Length != 1)
            {
                return;
            }

            StartCoroutine(StartSimulation(paths[0]));
        }

        private IEnumerator StartSimulation(string path)
        {
            BeginScreen.SetActive(false);

            iTween.RotateTo(gameObject, iTween.Hash("y", RotationEmptyView, "easeType", "easeInOutBack"));
            yield return new WaitForSeconds(1f);

            var fileStreamReader = new StreamReader(path);
            var fileContent = fileStreamReader.ReadToEnd();
            var resultTranferObjectFromFileResultTranferObject = JsonUtility.FromJson<ResultTranferObject>(fileContent);
            fileStreamReader.Close();

            var settings = LearningSettings.Instance;
            settings = resultTranferObjectFromFileResultTranferObject.Settings;

            SceneManager.LoadScene("Testing");
        }

        private List<int> CreateListOfLayers()
        {
            return LayerSliders.Select(slider => (int) slider.currentValue).Where(value => value > 0).ToList();
        }

        private ActivationFunctionTypes FunctionTypeFromString(string input)
        {
            switch (input)
            {
                case "Linear":
                    return ActivationFunctionTypes.Linear;
                case "Sigmoid":
                    return ActivationFunctionTypes.Sigmoid;
                case "Signum":
                    return ActivationFunctionTypes.Signum;
                case "Hyperbolic Tangent":
                    return ActivationFunctionTypes.Tanh;
                default:
                    return ActivationFunctionTypes.Linear;
            }
        }

        private ParentSelectionMethods ParentSelectionMethodFromString(String input)
        {
            switch (input)
            {
                case "Random Selection":
                    return ParentSelectionMethods.Random;
                case "Cubic Selection":
                    return ParentSelectionMethods.Cubic;
                case "Roulette Wheel":
                    return ParentSelectionMethods.RouletteWheel;
                case "Tournament Selection":
                    return ParentSelectionMethods.Tournament;
                default:
                    return ParentSelectionMethods.Tournament;
            }
        }
    }
}
