﻿using UnityEngine;
using UnityEngine.UI;

namespace Bilinski.Code.UI
{
    public class BillboardSystem : MonoBehaviour
    {
        public Text TimeoutText;
        public Text GenerationText;
        public Text CurrentBestFitnessText;
        public Text BestFitnessText;

        private float _timeOut;
        private int _currentGeneration;
        private int _currentBestFitness;
        private int _bestFitness;

        void FixedUpdate ()
        {
            UpdateUserInterface();
        }

        public void NotifyAboutNewPossibleBestFitness(int fitness)
        {
            if (fitness > _currentBestFitness)
            {
                _currentBestFitness = fitness;
            }
        }

        public void ResetFitness()
        {
            _currentBestFitness = 0;
        }

        public void UpdateBasicData(float timeout, int currentGeneration)
        {
            _timeOut = timeout;
            _currentGeneration = currentGeneration;

            if (_currentBestFitness > _bestFitness)
            {
                _bestFitness = _currentBestFitness;
            }
        }

        private void UpdateUserInterface()
        {
            TimeoutText.text = Mathf.Round(_timeOut).ToString();
            GenerationText.text = _currentGeneration.ToString();
            CurrentBestFitnessText.text = _currentBestFitness.ToString();
            BestFitnessText.text = _bestFitness.ToString();
        }
    }
}
