﻿using Bilinski.Code.AI;
using UnityEngine;
using UnityEngine.UI;

namespace Bilinski.Code.UI
{
    public class AdvancedExitMenu : MonoBehaviour
    {
        public GameObject[] MenuItems;
        public Material BlurMaterial;
        public float BlurTimeout;
        public AISupervisor Supervisor;

        private float _animationProgress;
        private bool _isMenuActivated;

        void Update ()
        {
            CaptureKey();
            CalculateAnimation();
        }

        private void SetMenuVisibility(bool visible)
        {
            foreach (var menu in MenuItems)
            {
                menu.SetActive(visible);
            }
        }

        private void CaptureKey()
        {
            if (!Input.GetKeyDown(KeyCode.Escape))
            {
                return;
            }

            EscapeButtonPressed();
        }

        private void CalculateAnimation()
        {
            if (_isMenuActivated)
            {
                _animationProgress += Time.deltaTime * (1 / BlurTimeout);
                _animationProgress = Mathf.Min(1f, _animationProgress);
            }
            else
            {
                _animationProgress -= Time.deltaTime * (1 / BlurTimeout);
                _animationProgress = Mathf.Max(0f, _animationProgress);
            }

            BlurMaterial.SetFloat("_EffectScale", _animationProgress);
            foreach (var menuItem in MenuItems)
            {
                var textItem = menuItem.GetComponent<Text>();
                if (textItem != null)
                {
                    textItem.color = new Color(textItem.color.r, textItem.color.g, textItem.color.b,
                        Mathf.Min(_animationProgress * 3f, 1f));
                }
            }
        }

        public void EscapeButtonPressed()
        {
            _isMenuActivated = !_isMenuActivated;
            SetMenuVisibility(_isMenuActivated);
        }

        public void SkipGenerationPressed()
        {
            Supervisor.SkipGeneration();
        }

        public void ExitPressed()
        {
            Application.Quit();
        }
    }
}
