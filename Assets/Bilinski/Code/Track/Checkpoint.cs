﻿using Bilinski.Code.Vehicle;
using UnityEngine;

namespace Bilinski.Code.Track
{
    public class Checkpoint : MonoBehaviour
    {
        public Checkpoint NextCheckpoint;

        void OnDrawGizmosSelected()
        {
            if (NextCheckpoint != null)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawLine(transform.position, NextCheckpoint.transform.position);
            }
        }

        void OnTriggerEnter(Collider other)
        {
            var carController = other.GetComponentInParent<AICarController>();

            if (carController != null)
            {
                carController.AddFitnessPoint(this, NextCheckpoint);
            }
        }
    }
}
