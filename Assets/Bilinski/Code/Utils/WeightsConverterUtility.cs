﻿using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Bilinski.Code.Utils
{
    public static class WeightsConverterUtility
    {
        private const char Separator1 = ' ';
        private const char Separator2 = '|';
        private const char Separator3 = '?';

        public static string Serialize(float [][,] inputData)
        {
            var output = new StringBuilder();

            foreach (var weights in inputData)
            {
                var xSize = weights.GetLength(0);
                var ySize = weights.GetLength(1);

                for (var x = 0; x < xSize; x++)
                {
                    for (var y = 0; y < ySize; y++)
                    {
                        output.Append(weights[x, y]);
                        output.Append(Separator1);
                    }

                    output.Append(Separator2);
                }

                output.Append(Separator3);
            }

            return output.ToString();
        }

        public static float[][,] Deserialize(string inputData)
        {
            var weightsString = Regex.Split(inputData, @"\" + Separator3);
            var output = new float[weightsString.Length - 1][,];

            for (var i = 0; i < weightsString.Length - 1; i++)
            {
                var weightsArraySplitedOnX = Regex.Split(weightsString[i], @"\" + Separator2);
                var ySize = weightsArraySplitedOnX[0].Count(val => val == Separator1);

                output[i] = new float[weightsArraySplitedOnX.Length - 1, ySize];

                for (var x = 0; x < weightsArraySplitedOnX.Length - 1; x++)
                {
                    var weightsArraySplitedOnY = Regex.Split(weightsArraySplitedOnX[x], @"\" + Separator1);

                    for (var y = 0; y < ySize; y++)
                    {
                        output[i][x, y] = float.Parse(weightsArraySplitedOnY[y]);
                    }
                }
            }

            return output;
        }
    }
}
