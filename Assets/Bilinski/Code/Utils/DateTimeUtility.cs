﻿using System;
using System.Globalization;

namespace Bilinski.Code.Utils
{
    public static class DateTimeUtility
    {
        private const string CultureName = "en-GB";

        private static CultureInfo _cultureInfo;

        public static string GetCurrentDateAndTime()
        {
            if (_cultureInfo == null)
            {
                _cultureInfo = new CultureInfo(CultureName);
            }

            return DateTime.Now.ToString(_cultureInfo);
        }
    }
}
